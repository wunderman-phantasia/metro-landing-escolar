'use strict';

var pngquant = require('imagemin-pngquant');
var mozjpeg = require('imagemin-mozjpeg');
var gifsicle = require('imagemin-gifsicle');
var svgo = require('imagemin-svgo');

module.exports = function (grunt) {
    // Project configuration.
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        jade: {
            options:{
              data: {
                info : grunt.file.readJSON('data.json')
              },
              filters: {
                sass: function(block){
                  return require('node-sass').renderSync(block);
                }
              }
            },
            debug: {
                options: {
                    pretty: true
                },
                files: [
                    {
                        expand: true,
                        cwd: '<%= pkg.dev.jade %>',
                        dest: '<%= pkg.prod.html %>',
                        src: '*.jade',
                        ext: '.html'
                    }
                ]
            },
            release: {
                files: [
                    {
                        expand: true,
                        cwd: '<%= pkg.dev.jade %>',
                        dest: '<%= pkg.prod.html %>',
                        src: '*.jade',
                        ext: '.html'
                    }
                ]
            }
        },
        sass: {
            debug: {
                options: {
                    style: 'expanded',
                    compass: true
                },
                files: [{
                    expand: true,
                    cwd: '<%= pkg.dev.sass %>',
                    src: ['*.scss'],
                    dest: '<%= pkg.prod.csstmp %>',
                    ext: '.css'
                  }]
            },
            release: {
                options:{
                    style: 'compressed',
                    compass: true
                    },
                files: [{
                    expand: true,
                    cwd: '<%= pkg.dev.sass %>',
                    src: ['*.scss'],
                    dest: '<%= pkg.prod.csstmp %>',
                    ext: '.css'
                  }]
            }
        },
        autoprefixer: {
          options: {
            browsers: ['ie 8', 'ie 9','opera 12', 'ff 15', 'chrome 25'],
            map: false,
            safe: true,
            diff: false
          },
          fixer: {
              expand: true,
              flatten: true,
              cwd: '<%= pkg.prod.csstmp %>',
              src: ['*.css'],
              dest: '<%= pkg.prod.css %>'
          }
        },
        cssmin: {
            options: {
                shorthandCompacting: false,
                roundingPrecision: -1,
                compatibility: 'ie8',
                /*keepSpecialComments: '*',*/
                sourceMap: true,
                advanced: false
            },
            "compresscss": {
                files: [{
                    expand: true,
                    cwd: '<%= pkg.prod.css %>',
                    src: ['*.css'],
                    dest: '<%= pkg.prod.css %>',
                    ext: '.min.css'
                  }]
            }
        },
        coffee: {
            options:{
                bare:true,
            },
            debug: {
                options: {
                  join: true
                },
                files: [{
                    expand: true,
                    cwd: '<%= pkg.dev.coffee %>',
                    src: ['*.coffee'],
                    dest: '<%= pkg.prod.js %>',
                    ext: '.js'
                }]
            },
            release: {
                files: [{
                    expand: true,
                    cwd: '<%= pkg.dev.coffee %>',
                    src: ['*.coffee'],
                    dest: '<%= pkg.prod.js %>',
                    ext: '.js'
                }]
            }
        },
        concat: {
            options: {
                separator: ';',
            },
            concatjs: {
                src: '<%= pkg.prod.filesjs %>',
                dest: '<%= pkg.prod.js %><%= pkg.name %>.js',
            },
        },
        uglify: {
            options: {
              banner: '/*! <%= pkg.name %> <%= pkg.author %> <%= grunt.template.today("yyyy-mm-dd") %> */',
              compress: {
                  "drop_console": true,
                  "hoist_funs": false,
                  "loops": false,
                  "unused": false
              },
              "mangle": false,
              "sourceMap": true,
              "sourceMapName": "<%= pkg.prod.js %><%= pkg.name %>.min.map",
              "report": "min",
              "beautify": {
                  "ascii_only": true
              },
              "ASCIIOnly":true,
              "quoteStyle":2
            },
            compressjs: {
                files: {
                    "<%= pkg.prod.js %><%= pkg.name %>.min.js": ["<%= pkg.prod.js %><%= pkg.name %>.js"]
                }
            }
        },
        imagemin:{
            options:{
                optimizationLevel: 5,
                use: [mozjpeg(),
                      pngquant({ quality: '65-80', speed: 4 }),
                      gifsicle({ interlaced: true }),
                      svgo()]
            },
            release: {
                files:[{
                    expand: true,
                    cwd: '<%= pkg.vendor.images %>',
                    src: ['*.{png,jpg,gif,svg}'],
                    dest: '<%= pkg.prod.images %>'
                }]
            }
        },
        copy: {
          main: {
            files: [
                {expand: true, cwd: '<%= pkg.vendor.css %>', src: ['**'], dest: '<%= pkg.prod.css %>/vendor'},
                {expand: true, cwd: '<%= pkg.vendor.font %>', src: ['**'], dest: '<%= pkg.prod.css %>/font'},
                {expand: true, cwd: '<%= pkg.vendor.js %>', src: ['**'], dest: '<%= pkg.prod.js %>/vendor'},
            ],
          },
        },
        clean: {
          buildcss: {
            src: '<%= pkg.prod.cleancss %>'
          },
          buildjs:{
            src:'<%= pkg.prod.filesjs %>'
          }
        },
        watch: {
            options: {
                livereload: true,
                nospawn: true,
                event: ['changed'],
            },
            jadewatch: {
                files: ['<%= pkg.dev.jade %>/*.jade', '<%= pkg.dev.jade %>/layout/*.jade', '<%= pkg.dev.jade %>/layout/includes/*.jade'],
                tasks: ['jade:debug']
            },
            sasswatch: {
                files: ['<%= pkg.dev.sass %>/*.scss'],
                tasks: ['clean:buildcss','sass:debug','autoprefixer','cssmin'],
                options: {
                    livereload: true,
                }
            },
            coffeewatch: {
                files: ['<%= pkg.dev.coffee %>/*.coffee'],
                tasks: ['coffee:debug','concat:concatjs','uglify:compressjs','clean:buildjs']
            }
        },
        connect: {
            server: {
                options: {
                    livereload:true,
                    port: 4000,
                    hostname: '*',
                    base: {
                        path: '<%= pkg.prod.html %>',
                        options: {
                            index: 'index.html',
                            maxAge: 300000
                        }
                    }
                }
            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-autoprefixer');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-coffee');
    grunt.loadNpmTasks("grunt-contrib-uglify");
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-jade');
    grunt.loadNpmTasks('grunt-contrib-copy');
    //grunt.loadNpmTasks('grunt-coffeelint');
    grunt.loadNpmTasks('grunt-contrib-imagemin');
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-contrib-connect');

    // Definicion de tareas
    grunt.registerTask('default', ['connect','watch']);
    grunt.registerTask('buildscss',['clean:buildcss','sass:debug','autoprefixer','cssmin']);
    grunt.registerTask('load', ['copy','imagemin','jade:debug','coffee:debug','buildscss']);
    grunt.registerTask('vendor', ['copy']);
    grunt.registerTask('image', ['imagemin']);
    grunt.registerTask('produccion', ['jade:release', 'sass:release', 'coffee:release']);

};
