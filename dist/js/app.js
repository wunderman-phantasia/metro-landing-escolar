var social;

social = (function() {
  function social() {
    var self;
    self = this;
    self.initialize();
  }

  social.prototype.initialize = function() {
    var self;
    self = this;
    self.events();
  };

  social.prototype.events = function() {
    var self;
    self = this;
    $('.shared_fb').on('click', function(evt) {
      evt.preventDefault();
      return self.openPopupWindow($(this).attr('href'), '', '550', '400');
    });
    return $('.shared_tw').on('click', function(evt) {
      evt.preventDefault();
      return self.openPopupWindow($(this).attr('href'), '', '550', '400');
    });
  };

  social.prototype.openPopupWindow = function(url, params, width, height) {
    var left, top;
    if (width !== 'undefined') {
      left = (screen.width / 2) - (width / 2);
      top = (screen.height / 2) - (height / 2);
      params += ",top=" + top + "px,left=" + left + "px";
    }
    return window.open(url, "", params + ",width=" + width + ",height=" + height + "");
  };

  return social;

})();
;(function() {
  var control, h;
  control = new social();
  h = window.innerHeight;
  if (h < 621) {
    $('.boxgirl').css('height', '475');
  }
  $('.legal').on("click", function(e) {
    e.preventDefault();
    $('.boxmodal').fadeIn(130);
    return $('#scrollbar1').tinyscrollbar({
      thumbSize: 100
    });
  });
  $('.btncerrar').on("click", function(e) {
    e.preventDefault();
    return $('.boxmodal').fadeOut(130);
  });
})();

$(window).on('resize', function() {
  var h;
  h = window.innerHeight;
  if (h < 621) {
    $('.boxgirl').css('height', '475');
  } else {
    $('.boxgirl').removeAttr('style');
  }
});
