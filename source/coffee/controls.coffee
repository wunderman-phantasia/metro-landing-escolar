class social
  constructor: ()->
    self = @
    self.initialize()

  initialize: ->
    self = @
    self.events()
    return

  events:()->
    self =@
    $('.shared_fb').on 'click', (evt)->
      evt.preventDefault();
      ##ga('send', 'event','Landing_escolar_metro_2016','General','btn_compartir_en_facebook']);
      self.openPopupWindow($(this).attr('href'), '', '550', '400');

    $('.shared_tw').on 'click', (evt)->
      evt.preventDefault();
      ##ga('send', 'event','Landing_escolar_metro_2016','General','btn_compartir_en_twitter');
      self.openPopupWindow($(this).attr('href'), '', '550', '400');

  openPopupWindow:(url, params, width, height)->
    if (width isnt 'undefined')
      left = (screen.width / 2) - (width / 2);
      top = (screen.height / 2) - (height / 2);
      params += ",top=" + top + "px,left=" + left + "px";
    window.open(url, "", params + ",width=" + width + ",height=" + height + "");