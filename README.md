Shell
======
step 01:
```sh
$ [sudo] npm install
```
step 02:
```sh
$ grunt load
```
step 03:
```sh
$ grunt
```

Link Form API
=============

dev

	http://apiforms.dev2.phantasia.pe/api/registro/grabar
	http://apiforms.dev2.phantasia.pe/api/registro/validar

Production



Host
=======
[http://127.0.0.1:4000](http://127.0.0.1:4000)

Link Helper
============
- [html convert Jade](http://html2jade.org/)
- [js convert coffee](http://js2.coffee/)
- [CSS convert SASS](http://css2sass.herokuapp.com/)
- [markdown editor](http://dillinger.io/)